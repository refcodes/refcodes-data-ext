// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data.ext.corporate;

import java.io.InputStream;
import java.net.URL;

import org.refcodes.data.DataLocator;

/**
 * The {@link LogoPixmap} defines the pixmaps which can be retrieved by the
 * according {@link LogoPixmapUrlFactory} and
 * {@link LogoPixmapInputStreamFactory}.
 *
 * @author steiner
 */
public enum LogoPixmap implements DataLocator {

	// /////////////////////////////////////////////////////////////////////////
	// ENUMS:
	// /////////////////////////////////////////////////////////////////////////

	// @formatter:off
	REFCODES("/org/refcodes/data/ext/corporate/refcodes-logo.png"),
	
	FUNCODES("/org/refcodes/data/ext/corporate/funcodes-logo.png"),
	
	COMCODES("/org/refcodes/data/ext/corporate/comcodes-logo.png");
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _path;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new logo pixmap.
	 *
	 * @param aPath the path
	 */
	private LogoPixmap( String aPath ) {
		_path = aPath;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public URL getDataUrl() {
		return getClass().getResource( _path );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream getDataInputStream() {
		return LogoPixmap.class.getResourceAsStream( _path );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the enumeration element representing the given pixmap name
	 * (ignoring the case) or null if none was found.
	 * 
	 * @param aPixmapName The pixmap name for which to get the enumeration
	 *        element.
	 * 
	 * @return The enumeration element determined or null if none matching was
	 *         found.
	 */
	public static LogoPixmap fromName( String aPixmapName ) {
		for ( LogoPixmap eValue : values() ) {
			if ( eValue.name().equalsIgnoreCase( aPixmapName ) ) {
				return eValue;
			}
		}
		return null;
	}
}
