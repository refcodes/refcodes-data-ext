module org.refcodes.data.ext.corporate {
	requires transitive org.refcodes.data;
	requires transitive org.refcodes.factory;

	exports org.refcodes.data.ext.corporate;
}
