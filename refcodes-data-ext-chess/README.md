# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***The `refcodes-data-ext` repository provides resources such as images, bitmaps or sounds grouped by "topic".***

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-data-ext-chess</artifactId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-data-ext). <s>Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-chess)</s> (not applicable).

## Contribution guidelines ##

* Writing tests
* Code review
* Adding functionality
* Fixing bugs

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The resources in the "`chess`" folder  are images as published by the [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page). [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) is a freely licensed media file repository. The resources found at [refcodes-data-ext-chess/src/main/resources/org/refcodes/data/ext/chess](https://bitbucket.org/refcodes/refcodes-data-ext/src/master/refcodes-data-ext-chess/src/main/resources/org/refcodes/data/ext/chessmen/)  ("`chessmen`") are images converted from `SVG` to `FXML` to be directly usable by `JavaFX`. The original `SVG` files and therewith the therefrom derived `FXML` files are published under the [following licenses](https://bitbucket.org/refcodes/refcodes-checkerboard/src/master/src/main/resources/chessmen/LICENSING.md). Please make sure to read and regard them licensing terms and conditions when using them images in your own work.

> For details on each image, please see [Category:SVG chess pieces](https://commons.wikimedia.org/wiki/Category%3ASVG_chess_pieces) at [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page).

The files in this folder are published under the following licenses, please make sure to read them licensing terms and conditions when using them images in your own work:

### GNU Free Documentation License ###

Permission is granted to copy, distribute and/or modify this document under the terms of the [GNU Free Documentation License](https://en.wikipedia.org/wiki/GNU_Free_Documentation_License), Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is included in the section entitled GNU Free Documentation License.

### Creative Commons ###

This file is licensed under the [Creative Commons](https://en.wikipedia.org/wiki/Creative_Commons) [Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license. 	

You are free:

* **to share** – to copy, distribute and transmit the work
* **to remix** – to adapt the work

Under the following conditions:

* **attribution** – You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
* **share alike** – If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

This licensing tag was added to these files as part of the GFDL [licensing update](https://meta.wikimedia.org/wiki/Licensing_update).

### BSD / Copyright <The author> ###

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3. Neither the name of The author nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### GNU General Public License ###

This work is [free software](https://en.wikipedia.org/wiki/Free_software); you can redistribute it and/or modify it under the terms of the **[GNU General Public License](https://en.wikipedia.org/wiki/GNU_General_Public_License)** as published by the [Free Software Foundation](https://en.wikipedia.org/wiki/Free_Software_Foundation); either version 2 of the License, or any later version. This work is distributed in the hope that it will be useful, but **without any warranty**; without even the implied warranty of **merchantability** or **fitness for a particular purpose**. See [version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) and [version 3 of the GNU General Public License](https://www.gnu.org/copyleft/gpl-3.0.html) for more details.
