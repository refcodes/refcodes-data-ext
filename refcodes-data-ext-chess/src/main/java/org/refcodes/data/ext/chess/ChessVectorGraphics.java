// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data.ext.chess;

import java.io.InputStream;
import java.net.URL;

import org.refcodes.data.DataLocator;

/**
 * The {@link ChessVectorGraphics} defines the pixmaps which can be retrieved by
 * the according {@link ChessVectorGraphicsUrlFactory} and
 * {@link ChessVectorGraphicsInputStreamFactory}.
 *
 * @author steiner
 */
public enum ChessVectorGraphics implements DataLocator {

	// /////////////////////////////////////////////////////////////////////////
	// ENUMS:
	// /////////////////////////////////////////////////////////////////////////

	// @formatter:off
	FXML_BDT_BISHOP_DARK_TRANSPARENT          ("/org/refcodes/data/ext/chess/bdt_bishop_dark_transparent.fxml"),
	SVG_BDT_BISHOP_DARK_TRANSPARENT           ("/org/refcodes/data/ext/chess/bdt_bishop_dark_transparent.svg"),
	FXML_BDT_BISHOP_ROTATED_DARK_TRANSPARENT  ("/org/refcodes/data/ext/chess/Bdt_bishop_rotated_dark_transparent.fxml"),
	SVG_BDT_BISHOP_ROTATED_DARK_TRANSPARENT   ("/org/refcodes/data/ext/chess/Bdt_bishop_rotated_dark_transparent.svg"),
	FXML_BLT_BISHOP_LIGHT_TRANSPARENT         ("/org/refcodes/data/ext/chess/blt_bishop_light_transparent.fxml"),
	SVG_BLT_BISHOP_LIGHT_TRANSPARENT          ("/org/refcodes/data/ext/chess/blt_bishop_light_transparent.svg"),
	FXML_BLT_BISHOP_ROTATED_LIGHT_TRANSPARENT ("/org/refcodes/data/ext/chess/Blt_bishop_rotated_light_transparent.fxml"),
	SVG_BLT_BISHOP_ROTATED_LIGHT_TRANSPARENT  ("/org/refcodes/data/ext/chess/Blt_bishop_rotated_light_transparent.svg"),
	FXML_EDT_BISHOP_ROTATED_DARK_TRANSPARENT  ("/org/refcodes/data/ext/chess/edt_bishop_rotated_dark_transparent.fxml"),
	SVG_EDT_BISHOP_ROTATED_DARK_TRANSPARENT   ("/org/refcodes/data/ext/chess/edt_bishop_rotated_dark_transparent.svg"),
	FXML_ELT_BISHOP_ROTATED_LIGHT_TRANSPARENT ("/org/refcodes/data/ext/chess/elt_bishop_rotated_light_transparent.fxml"),
	SVG_ELT_BISHOP_ROTATED_LIGHT_TRANSPARENT  ("/org/refcodes/data/ext/chess/elt_bishop_rotated_light_transparent.svg"),
	FXML_FDT_KING_ROTATED_DARK_TRANSPARENT    ("/org/refcodes/data/ext/chess/fdt_king_rotated_dark_transparent.fxml"),
	SVG_FDT_KING_ROTATED_DARK_TRANSPARENT     ("/org/refcodes/data/ext/chess/fdt_king_rotated_dark_transparent.svg"),
	FXML_FLT_KING_ROTATED_LIGHT_TRANSPARENT   ("/org/refcodes/data/ext/chess/flt_king_rotated_light_transparent.fxml"),
	SVG_FLT_KING_ROTATED_LIGHT_TRANSPARENT    ("/org/refcodes/data/ext/chess/flt_king_rotated_light_transparent.svg"),
	FXML_GDT_QUEEN_ROTATED_DARK_TRANSPARENT   ("/org/refcodes/data/ext/chess/gdt_queen_rotated_dark_transparent.fxml"),
	SVG_GDT_QUEEN_ROTATED_DARK_TRANSPARENT    ("/org/refcodes/data/ext/chess/gdt_queen_rotated_dark_transparent.svg"),
	FXML_GLT_QUEEN_ROTATED_LIGHT_TRANSPARENT  ("/org/refcodes/data/ext/chess/glt_queen_rotated_light_transparent.fxml"),
	SVG_GLT_QUEEN_ROTATED_LIGHT_TRANSPARENT   ("/org/refcodes/data/ext/chess/glt_queen_rotated_light_transparent.svg"),
	FXML_HDT_PAWN_ROTATED_DARK_TRANSPARENT    ("/org/refcodes/data/ext/chess/hdt_pawn_rotated_dark_transparent.fxml"),
	SVG_HDT_PAWN_ROTATED_DARK_TRANSPARENT     ("/org/refcodes/data/ext/chess/hdt_pawn_rotated_dark_transparent.svg"),
	FXML_HLT_PAWN_ROTATED_LIGHT_TRANSPARENT   ("/org/refcodes/data/ext/chess/hlt_pawn_rotated_light_transparent.fxml"),
	SVG_HLT_PAWN_ROTATED_LIGHT_TRANSPARENT    ("/org/refcodes/data/ext/chess/hlt_pawn_rotated_light_transparent.svg"),
	FXML_KDT_KING_DARK_TRANSPARENT            ("/org/refcodes/data/ext/chess/kdt_king_dark_transparent.fxml"),
	SVG_KDT_KING_DARK_TRANSPARENT             ("/org/refcodes/data/ext/chess/kdt_king_dark_transparent.svg"),
	FXML_KLT_KING_LIGHT_TRANSPARENT           ("/org/refcodes/data/ext/chess/klt_king_light_transparent.fxml"),
	SVG_KLT_KING_LIGHT_TRANSPARENT            ("/org/refcodes/data/ext/chess/klt_king_light_transparent.svg"),
	FXML_MDT_ROOK_ROTATED_DARK_TRANSPARENT    ("/org/refcodes/data/ext/chess/mdt_rook_rotated_dark_transparent.fxml"),
	SVG_MDT_ROOK_ROTATED_DARK_TRANSPARENT     ("/org/refcodes/data/ext/chess/mdt_rook_rotated_dark_transparent.svg"),
	FXML_MLT_ROOK_ROTATED_LIGHT_TRANSPARENT   ("/org/refcodes/data/ext/chess/mlt_rook_rotated_light_transparent.fxml"),
	SVG_MLT_ROOK_ROTATED_LIGHT_TRANSPARENT    ("/org/refcodes/data/ext/chess/mlt_rook_rotated_light_transparent.svg"),
	FXML_NDT_KNIGHT_DARK_TRANSPARENT          ("/org/refcodes/data/ext/chess/ndt_knight_dark_transparent.fxml"),
	SVG_NDT_KNIGHT_DARK_TRANSPARENT           ("/org/refcodes/data/ext/chess/ndt_knight_dark_transparent.svg"),
	FXML_NDT_KNIGHT_ROTATED_DARK_TRANSPARENT  ("/org/refcodes/data/ext/chess/Ndt_knight_rotated_dark_transparent.fxml"),
	SVG_NDT_KNIGHT_ROTATED_DARK_TRANSPARENT   ("/org/refcodes/data/ext/chess/Ndt_knight_rotated_dark_transparent.svg"),
	FXML_NLT_KNIGHT_LIGHT_TRANSPARENT         ("/org/refcodes/data/ext/chess/nlt_knight_light_transparent.fxml"),
	SVG_NLT_KNIGHT_LIGHT_TRANSPARENT          ("/org/refcodes/data/ext/chess/nlt_knight_light_transparent.svg"),
	FXML_NLT_KNIGHT_ROTATED_LIGHT_TRANSPARENT ("/org/refcodes/data/ext/chess/Nlt_knight_rotated_light_transparent.fxml"),
	SVG_NLT_KNIGHT_ROTATED_LIGHT_TRANSPARENT  ("/org/refcodes/data/ext/chess/Nlt_knight_rotated_light_transparent.svg"),
	FXML_PDT_PAWN_DARK_TRANSPARENT            ("/org/refcodes/data/ext/chess/pdt_pawn_dark_transparent.fxml"),
	SVG_PDT_PAWN_DARK_TRANSPARENT             ("/org/refcodes/data/ext/chess/pdt_pawn_dark_transparent.svg"),
	FXML_PLT_PAWN_LIGHT_TRANSPARENT           ("/org/refcodes/data/ext/chess/plt_pawn_light_transparent.fxml"),
	SVG_PLT_PAWN_LIGHT_TRANSPARENT            ("/org/refcodes/data/ext/chess/plt_pawn_light_transparent.svg"),
	FXML_QDT_QUEEN_DARK_TRANSPARENT           ("/org/refcodes/data/ext/chess/qdt_queen_dark_transparent.fxml"),
	SVG_QDT_QUEEN_DARK_TRANSPARENT            ("/org/refcodes/data/ext/chess/qdt_queen_dark_transparent.svg"),
	FXML_QLT_QUEEN_LIGHT_TRANSPARENT          ("/org/refcodes/data/ext/chess/qlt_queen_light_transparent.fxml"),
	SVG_QLT_QUEEN_LIGHT_TRANSPARENT           ("/org/refcodes/data/ext/chess/qlt_queen_light_transparent.svg"),
	FXML_RDT_ROOK_DARK_TRANSPARENT            ("/org/refcodes/data/ext/chess/rdt_rook_dark_transparent.fxml"),
	SVG_RDT_ROOK_DARK_TRANSPARENT             ("/org/refcodes/data/ext/chess/rdt_rook_dark_transparent.svg"),
	FXML_RLT_ROOK_LIGHT_TRANSPARENT           ("/org/refcodes/data/ext/chess/rlt_rook_light_transparent.fxml"),
	SVG_RLT_ROOK_LIGHT_TRANSPARENT            ("/org/refcodes/data/ext/chess/rlt_rook_light_transparent.svg"),
	FXML_SDT_KNIGHT_ROTATED_DARK_TRANSPARENT  ("/org/refcodes/data/ext/chess/sdt_knight_rotated_dark_transparent.fxml"),
	SVG_SDT_KNIGHT_ROTATED_DARK_TRANSPARENT   ("/org/refcodes/data/ext/chess/sdt_knight_rotated_dark_transparent.svg"),
	FXML_SLT_KNIGHT_ROTATED_LIGHT_TRANSPARENT ("/org/refcodes/data/ext/chess/slt_knight_rotated_light_transparent.fxml"),
	SVG_SLT_KNIGHT_ROTATED_LIGHT_TRANSPARENT  ("/org/refcodes/data/ext/chess/slt_knight_rotated_light_transparent.svg");
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _path;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new chess vector graphics.
	 *
	 * @param aPath the path
	 */
	private ChessVectorGraphics( String aPath ) {
		_path = aPath;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public URL getDataUrl() {
		return getClass().getResource( _path );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream getDataInputStream() {
		return ChessVectorGraphics.class.getResourceAsStream( _path );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the enumeration element representing the given vector graphics
	 * name (ignoring the case) or null if none was found.
	 * 
	 * @param aPixmapName The pixmap name for which to get the enumeration
	 *        element.
	 * 
	 * @return The enumeration element determined or null if none matching was
	 *         found.
	 */
	public static ChessVectorGraphics fromName( String aPixmapName ) {
		for ( ChessVectorGraphics eValue : values() ) {
			if ( eValue.name().equalsIgnoreCase( aPixmapName ) ) {
				return eValue;
			}
		}
		return null;
	}
}
