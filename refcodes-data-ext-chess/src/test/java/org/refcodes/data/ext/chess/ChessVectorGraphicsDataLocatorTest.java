// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data.ext.chess;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.junit.jupiter.api.Test;

/**
 * The Class ChessVectorGraphicsDataLocatorTest.
 *
 * @author steiner
 */
public class ChessVectorGraphicsDataLocatorTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAllInputStreams() throws IOException {
		final ChessVectorGraphicsInputStreamFactory theVectorGraphicsFactory = new ChessVectorGraphicsInputStreamFactory();
		for ( ChessVectorGraphics eVectorGraphics : ChessVectorGraphics.values() ) {
			final InputStream eDataInputStream = theVectorGraphicsFactory.create( eVectorGraphics );
			assertNotNull( eDataInputStream );
			eDataInputStream.read();
		}
	}

	@Test
	public void testAllUrls() throws IOException {
		final ChessVectorGraphicsUrlFactory theVectorGraphicsFactory = new ChessVectorGraphicsUrlFactory();
		for ( ChessVectorGraphics eVectorGraphics : ChessVectorGraphics.values() ) {
			final URL eUrl = theVectorGraphicsFactory.create( eVectorGraphics );
			assertNotNull( eUrl );
		}
	}
}
