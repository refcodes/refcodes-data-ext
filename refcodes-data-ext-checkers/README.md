# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***The `refcodes-data-ext` repository provides resources such as images, bitmaps or sounds grouped by "topic".***

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-data-ext-checkers</artifactId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-data-ext). <s>Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-data-ext-checkers)</s> (not applicable).

## Contribution guidelines ##

* Writing tests
* Code review
* Adding functionality
* Fixing bugs

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The resources *except* the ones prefixed `othello` in the "`checkers`" folder  are images as published by the [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page). [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) is a freely licensed media file repository. The resources found at [refcodes-data-ext-checkers/src/main/resources/org/refcodes/resource/ext/checkers](https://bitbucket.org/refcodes/refcodes-data-ext/src/master/refcodes-data-ext-checkers/src/main/resources/org/refcodes/resource/ext/checkers/)  ("`checkers`") are images converted from `SVG` to `FXML` to be directly usable by `JavaFX`. The original `SVG` files and therewith the therefrom derived `FXML` files are published under the [following licenses](https://bitbucket.org/refcodes/refcodes-checkerboard/src/master/src/main/resources/checkers/LICENSING.md). Please make sure to read and regard them licensing terms and conditions when using them images in your own work.

> For details on the images in question, please see [File:Draughts.svg](https://en.wikipedia.org/wiki/File%3ADraughts.svg) at [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page).

The files in question in taht folder are published under the following licenses, please make sure to read them licensing terms and conditions when using them images in your own work:

### GNU Free Documentation License ###

Permission is granted to copy, distribute and/or modify this document under the terms of the [GNU Free Documentation License](https://en.wikipedia.org/wiki/GNU_Free_Documentation_License), Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is included in the section entitled GNU Free Documentation License.

### Public domain ###

> "I, the copyright holder of this work, release this work into the [public domain](https://en.wikipedia.org/wiki/public_domain). This applies worldwide. In some countries this may not be legally possible; if so: I grant anyone the right to use this work for any purpose, without any conditions, unless such conditions are required by law."

---

The resources prefixed `othello` in the `checkers` folder are images as published by [All-free-download.com](http://all-free-download.com). See the according [Terms of Service](http://all-free-download.com/pages/term.html) as well as the according [License information](http://all-free-download.com/pages/licence.html). 

> For details on the images in question, please see [Othello Board Game clip art](http://all-free-download.com/free-vector/download/othello_board_game_clip_art_24003.html) at [All-free-download.com](http://all-free-download.com).