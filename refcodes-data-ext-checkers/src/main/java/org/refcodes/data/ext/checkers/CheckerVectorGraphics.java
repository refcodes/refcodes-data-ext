// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.data.ext.checkers;

import java.io.InputStream;
import java.net.URL;

import org.refcodes.data.DataLocator;

/**
 * The {@link CheckerVectorGraphics} defines the pixmaps which can be retrieved
 * by the according {@link CheckerVectorGraphicsUrlFactory} and
 * {@link CheckerVectorGraphicsInputStreamFactory}.
 *
 * @author steiner
 */
public enum CheckerVectorGraphics implements DataLocator {

	// /////////////////////////////////////////////////////////////////////////
	// ENUMS:
	// /////////////////////////////////////////////////////////////////////////

	// @formatter:off
	SVG_PIECE_GRAY("/org/refcodes/data/ext/checkers/othello_piece_gray.svg"),
	
	FXML_PIECE_GRAY("/org/refcodes/data/ext/checkers/othello_piece_gray.fxml"),
	
	SVG_PIECE_RED("/org/refcodes/data/ext/checkers/othello_piece_red.svg"),
	
	FXML_PIECE_RED("/org/refcodes/data/ext/checkers/othello_piece_red.fxml"),
	
	SVG_PIECE_WHITE("/org/refcodes/data/ext/checkers/othello_piece_white.svg"),
	
	FXML_PIECE_WHITE("/org/refcodes/data/ext/checkers/othello_piece_white.fxml");
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _path;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new checker vector graphics.
	 *
	 * @param aPath the path
	 */
	private CheckerVectorGraphics( String aPath ) {
		_path = aPath;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public URL getDataUrl() {
		return getClass().getResource( _path );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream getDataInputStream() {
		return CheckerVectorGraphics.class.getResourceAsStream( _path );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the enumeration element representing the given vector graphics
	 * name (ignoring the case) or null if none was found.
	 * 
	 * @param aPixmapName The pixmap name for which to get the enumeration
	 *        element.
	 * 
	 * @return The enumeration element determined or null if none matching was
	 *         found.
	 */
	public static CheckerVectorGraphics fromName( String aPixmapName ) {
		for ( CheckerVectorGraphics eValue : values() ) {
			if ( eValue.name().equalsIgnoreCase( aPixmapName ) ) {
				return eValue;
			}
		}
		return null;
	}
}
